INSERT INTO hosts
(hostid, host, email, local_image, license, profile, gpg)
VALUES(1, 'droops', 'droops.nospam@nospam.gmail.com', '1', 'CC-BY-NC-SA', 'nomicon.info','gpg1');
INSERT INTO hosts
(hostid, host, email, local_image, license, profile, gpg)
VALUES(3, 'dosman', 'dosman.nospam@nospam.packetsniffers.org', '0', 'CC-BY-SA', 'packetsniffers.org','gpg3');
INSERT INTO hosts
(hostid, host, email, local_image, license, profile, gpg)
VALUES(73, 'deepgeek', 'hpr.nospam@nospam.deepgeek.us', '0', 'CC-BY-SA', 'linkpot.net/vogueing/','gpg73');

-- Older than 5 weeks
INSERT INTO eps 
(id, hostid, explicit, date, license, title, summary, series, notes, downloads, tags, duration, valid)
VALUES(6, 3, 1, '2008-01-08', 'CC-BY-NC-SA', 'Part 15 Broadcasting', 'dosman and zach from the packetsniffers talk about Part 15 Broadcasting which is low power', '', '<p>dosman and zach from the packetsniffers talk about Part 15 Broadcasting which is low power broadcasting for the local area. Used to do community radio around an event, a church, concerts etc. They discuss what the regulations are in the US, what you need, how to get started, what things to consider. All in all great introduction to the topic.
</p>
<h2>links</h2>
<ul>
<li>Part 15 broadcasting resources: <a href="https://www.part15.us">https://www.part15.us</a></li>
<li>SSTRAN AMT3000 part 15 transmitter: <a href="https://www.sstran.com">https://www.sstran.com</a></li>
<li>FCC Low Power Broadcast Radio Stations: <a href="https://www.fcc.gov/guides/low-power-broadcast-radio-stations">https://www.fcc.gov/guides/low-power-broadcast-radio-stations</a></li>
<li>Windows Radio Broadcast software: <a href="https://www.zarastudio.es/en/ ">https://www.zarastudio.es/en/ </a></li>
</ul>',0,'Part 15, HAM, soldering, fcc, radio',2155,1);
INSERT INTO eps
(id, hostid, explicit, date, license, title, summary, series, notes, downloads, tags, duration, valid)
VALUES(35, 1, 1, '2008-02-18', 'CC-BY-NC-SA', 'An interview with John Whaley', 'droops interviews John Whaley from Moka5', 'Interviews', '<p>
droops interviews John Whaley from <a href="https://www.moka5.com/about-us/our-team/john-whaley/"> Moka5.</a>
</p>
<p>
John Whaley is responsible for the technical vision of Moka5. He holds a doctorate in computer science from Stanford University, where he made key contributions to the fields of program analysis, compilers, and virtual machines. He is the winner of numerous awards including the Arthur L. Samuel Thesis Award for Best Thesis at Stanford, and has worked at IBM’s T.J. Watson Research Center and Tokyo Research Lab. John was named one of the top 15 programmers in the USA Computing Olympiad. He also holds bachelor’s and master’s degrees in computer science from MIT and speaks fluent Japanese.
</p>',0,'Moka5,interview,"computer science"',1240,1);

--More than a week old, less than 5 weeks old
INSERT INTO eps
(id, hostid, explicit, date, license, title, summary, series, notes, downloads, tags,duration, valid)
VALUES(1236,73,1, date('now','-28 days'),'CC-BY-SA','Lament For httpd','Moving away from the thttpd web server','','<p>
DeepGeek gets all emotional about changing web servers at his web
co-op. "Hell, it''s just a tool." Not for DeepGeek, who equates moving
away from thttpd to the closing of an era! To him, "slick design" can
take a backseat to feelings of camaraderie from your fellows on the
intwebz any day of the week!
</p>
<p>
A few well-placed links...
</p>

<ul>
<li><a href="https://www.links.id.au/150/90s-web-design--a-nostalgic-look-back.html">https://www.links.id.au/150/90s-web-design--a-nostalgic-look-back.html</a>
</li>
<li><a href="https://en.wikipedia.org/wiki/Thttpd">https://en.wikipedia.org/wiki/Thttpd</a>
</li>
<li><a href="https://acme.com/software/thttpd/benchmarks.html">https://acme.com/software/thttpd/benchmarks.html</a>
</li>
<li><a href="https://www.hiawatha-webserver.org/">https://www.hiawatha-webserver.org/</a>
</li>
</ul>',0,'"web server",thttpd',428,1);

INSERT INTO eps
(id, hostid, explicit, date, license, title, summary, series, notes, downloads, tags, duration, valid)
VALUES (1459,73,1,date('now', '-14 days'),'CC-BY-SA','Locational Privacy with retrotech-the lowly pager','deepgeek advocates the use of a pager for privacy reasons','Privacy and Security','<p>
In this episode, deepgeek suggests that adding and old, and perhaps laughable
by modern standards, device to your mobile lifestyle.  Deepgeek reveals that
said device is the pager, but he eventually gives good reasons for doing so.
</p>
<p>
The primary reason is that the paging company does not know where you are, 
so they can''t tell "the man" where you are.  Other reasons are redundancy 
and trouble interpreting audio.  But in the end, you find out why first 
responders and medical and fire personal still use these devices, and how you, 
as a privacy lover, may reap benefits from using this technology also.
</p>
<p>
Some links mentioned in case you want to follow them...
</p>
<p>
Duck Duck Go search on locational privacy
<a href="https://duckduckgo.com/?q=locational+privacy">https://duckduckgo.com?q=locational+privacy</a>
</p>
<p>
"privacy is dead" audio
</p>

<ul>
<li><a href="https://www.hopenumbersix.net/mp3/16/privacy_is_dead.mp3">https://www.hopenumbersix.net/mp3/16/privacy_is_dead.mp3</a></li>
<li><a href="https://www.hopenumbersix.net/mp3/16/privacy1.mp3">https://www.hopenumbersix.net/mp3/16/privacy1.mp3</a></li>
<li><a href="https://www.hopenumbersix.net/mp3/16/privacy2.mp3">https://www.hopenumbersix.net/mp3/16/privacy2.mp3</a></li>
<li><a href="https://www.hopenumbersix.net/mp3/16/privacy3.mp3">https://www.hopenumbersix.net/mp3/16/privacy3.mp3</a></li>
</ul>

<p>
USA''s two remaining paging companies
</p>

<ul>
<li><a href="https://www.americanmessaging.net/">https://www.americanmessaging.net/</a></li>
<li><a href="https://www.usamobility.com/">https://www.usamobility.com/</a>
</ul>

<p>don''t forget to check out resellers for deals, like "free pager with one year prepaid</p>

<p>
A good sms via email webpage
</p>

<ul>
<li><a href="https://www.tech-recipes.com/rx/939/sms_email_cingular_nextel_sprint_tmobile_verizon_virgin/">https://www.tech-recipes.com/rx/939/sms_email_cingular_nextel_sprint_tmobile_verizon_virgin/</a></li>
</ul>',0,'pager,privacy',1138,1
);

--Within the last 7 days
INSERT INTO eps 
(id, hostid, explicit, date, license, title, summary, series, notes, downloads, tags, duration, valid)
VALUES (2376,73,0,date('now', '-3 days'),'CC-BY-SA','Information Underground: 21st Century Superstar','Deepgeek, Lostnbronx, and Klaatu talk about iconless culture','Information Underground','<p>Deepgeek, Lostnbronx, and Klaatu talk about cultural iconography.</p>',0,'culture, cultural icon',3180,1);
INSERT INTO eps
(id, hostid, explicit, date, license, title, summary, series, notes, downloads, tags, duration, valid)
VALUES (3276,73,0,date('now','-1 days'),'CC-BY-SA','Deepgeek''s thoughts about HD Radio','Klaatu reads a post by Deepgeek','Information Underground','<p><em>
Deepgeek, Klaatu, and Lostnbronx discuss things.
</em></p>
</header>
<p>
Deepgeek''s thoughts about HD radio.
</p>

<h2>Links</h2>

<ul>
<li><a href="https://hdradio.com/broadcasters/engineering-support/high-quality-consistent-multicast-engineering/" target="_blank">multicast chart</a></li>
<li><a href="https://en.wikipedia.org/wiki/Sacd" target="_blank">en.wikipedia.org&#47;wiki&#47;Sacd</a></li>
<li><a href="https://traffic.libsyn.com/secure/textfiles/Jason_Scott_Talks_His_Way_Out_Of_It_-_Episode_120.mp3" target="_blank">Jason Scott episode 120</a></li>
</ul>',0,'radio',612,1);
